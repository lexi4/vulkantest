﻿#include "../Vector2D.h"

FVector2D::FVector2D(const float& InX, const float& InY)
{
    X = InX;
    Y = InY;
}

FVector2D::FVector2D(const float& InAll)
{
    FVector2D(InAll,InAll);
}

bool FVector2D::operator==(const FVector2D& InSecond) const
{
    return this->X == InSecond.X; 
}

bool FVector2D::operator!=(const FVector2D& InSecond) const
{
    return !(*this == InSecond);
}

void FVector2D::operator=(const FVector2D& InSecond) const
{
    FVector2D(InSecond.X,InSecond.Y);
}
