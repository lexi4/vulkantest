﻿#pragma once

struct FVector2D
{
public:
    FVector2D() = default;
    FVector2D(const float& InX,const float& InY);
    FVector2D(const float& InAll = 0);

    float X;
    float Y;

public:
    bool operator==(const FVector2D& InSecond) const;
    bool operator!=(const FVector2D& InSecond) const;
    void operator=(const FVector2D& InSecond) const;
};
