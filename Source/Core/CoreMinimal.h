﻿#pragma once

#define _WIN32_WINDOWS 0x0601
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <MulticastDelegate.h>
#include <string>
#include <iostream>
#include <asio.hpp>
#include <set>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include "Time.h"
#include "Vectors/Vector2D.h"
#include "Vectors/Vector3D.h"

//Literals
typedef std::string_view FTypeName;
typedef std::string FString;
typedef char* FName;

//Math
typedef uint32_t uint32;
typedef int32_t int32;
typedef uint8_t uint8;
typedef int8_t int8;
typedef uint16_t uint16;
typedef int16_t int16;
typedef uint64_t uint64;
typedef int64_t int64;

template<class T>
using TArray = std::vector<T>;

template<class T>
using TSet = std::set<T>;

inline TArray<FString>* CharsToStrings(const char** Chars)
{
    TArray<FString>* Result = new TArray<FString>(Chars, Chars + (sizeof(Chars) / sizeof(*Chars)));
    return Result;
}

template<class T,class T2>
T* Cast(T2*& ClassInstancePointer)
{
    return dynamic_cast<T*>(ClassInstancePointer);
}

template<class T,class T2>
T* StaticCast(T2*& ClassInstancePointer)
{
    return static_cast<T*>(ClassInstancePointer);
}

template<class T>
void InitObject(T*& InValue)
{
    if(InValue == nullptr)
    {
        InValue = new T();
    }
}


namespace typeName {
    using namespace std;
    template <typename T>
     constexpr string_view wrapped_type_name () {
#ifdef __clang__
        return __PRETTY_FUNCTION__;
#elif defined(__GNUC__)
        return  __PRETTY_FUNCTION__;
#elif defined(_MSC_VER)
        return  __FUNCSIG__;
#endif
    }

    class probe_type;
    constexpr string_view probe_type_name ("typeName::probe_type");
    constexpr string_view probe_type_name_elaborated ("class typeName::probe_type");
    constexpr string_view probe_type_name_used (wrapped_type_name<probe_type> ().find (probe_type_name_elaborated) != -1 ? probe_type_name_elaborated : probe_type_name);

    constexpr size_t prefix_size () {
        return wrapped_type_name<probe_type> ().find (probe_type_name_used);
    }

    constexpr size_t suffix_size () {
        return wrapped_type_name<probe_type> ().length () - prefix_size () - probe_type_name_used.length ();
    }

    template <typename T>
    string_view type_name () {
        constexpr auto type_name = wrapped_type_name<T> ();

        return type_name.substr (prefix_size (), type_name.length () - prefix_size () - suffix_size ());
    }
}

#define LOG(Message) \
{ \
    std::cout << Time::GetTimeSinceStartUp() << " [" << __FUNCTION__ << "] " << FString(Message) << std::endl; \
} \

