﻿#pragma once 


#include <Render/Render.h>
#include <Render/Window/Window.h>
#include <CodeSandbox/UCodeSandbox.h>
#include <GLFW/glfw3.h>
#include <Core/Time.h>

int main() {
    LOG();
    URender* RenderInstance = new URender();

    //Input events
    while (!glfwWindowShouldClose(RenderInstance->Window->GlfwWindow)) {
        glfwPollEvents();
    }
    delete RenderInstance;
    return 0;
}

