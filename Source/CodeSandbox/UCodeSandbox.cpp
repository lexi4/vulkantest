﻿#include "UCodeSandbox.h"

#include "Samples/SandboxInterface.h"

#include "Samples/DelegateTest/DelegateTest.h"
#include "Samples/SortLevelsTest/MultipleSortTest.h"

#include <typeinfo>

void UCodeSandbox::Execute()
{
    UDelegateTest* DelegateTest = nullptr;
    UMultipleSortTest* MultipleSortTest = nullptr;

    TArray<ISandbox*> Cases {
        PrepareTest(DelegateTest),
        PrepareTest(MultipleSortTest),
    };

    for(ISandbox* TestCase : Cases)
    {
        std::cout << std::endl << "=== START PROCESSING:  " << TestCase->TypeName << " ===" << std::endl << std::endl;
        TestCase->Execute();
        std::cout << std::endl << "=== FINISHED:  " << TestCase->TypeName << " ===" << std::endl << std::endl;
    }
}
