﻿#pragma once
#include "Samples/SandboxInterface.h"
#include "Core/CoreMinimal.h"

class UCodeSandbox
{
public:
    void Execute();

protected:
    template<class T>
    ISandbox* PrepareTest(T*& InTest)
    {
        InitObject(InTest);

        ISandbox* TestCase = Cast<ISandbox>(InTest);
        TestCase->SetSanboxTestName(InTest);
        return TestCase;
    }
};
