﻿#pragma once
#include <map>
#include <random>
#include <Core/CoreMinimal.h>

#include "CodeSandbox/Samples/SandboxInterface.h"

#define NUM_PLAYERS 40

enum class EMarkerPrimary : uint8
{
    B = 0
   ,A = 1
   ,
};

enum class EMarkerSecondary : uint8
{
    C = 0
   ,B = 1 << 0
   ,A = 1 << 1
   ,
};

std::map<uint8, uint8> SecondaryMarkerOrderConfig = {
    {
        (uint8)EMarkerSecondary::C
      , 0
    }
  , {
        (uint8)EMarkerSecondary::C | (uint8)EMarkerSecondary::B
      , 1
    }
  , {
        (uint8)EMarkerSecondary::A
      , 2
    }
  , {
        (uint8)EMarkerSecondary::A | (uint8)EMarkerSecondary::B
      , 3
    }
   ,
};

struct FPlayer
{
    FString          Name = "";
    EMarkerPrimary   PrimaryMarker;
    EMarkerSecondary SecondaryMarker;
};

class UMultipleSortTest : public ISandbox
{
    TArray<FPlayer> Players;

public:
    void Execute() override
    {
        GeneratePlayers();
        SortPlayers();
        PrintPlayers();
    }

    void GeneratePlayers()
    {
        std::random_device              rd; // obtain a random number from hardware
        std::mt19937                    genPrimary(rd()); // seed the generator
        std::mt19937                    genSecondary(rd()); // seed the generator
        std::mt19937                    genName(rd()); // seed the generator
        std::uniform_int_distribution<> RandPrimary(0, 1); // define the range
        std::uniform_int_distribution<> RandSecondary(0, 3); // define the range
        std::uniform_int_distribution<> RandName(0, 19); // define the range

        TArray<FString> Names = {
            "a"
          , "b"
          , "c"
          , "d"
          , "e"
          , "f"
          , "g"
          , "h"
          , "i"
          , "j"
          , "k"
          , "l"
          , "m"
          , "o"
          , "p"
          , "q"
          , "r"
          , "s"
          , "t"
          , "u"
        };

        Players.clear();

        TArray<uint8> SecondaryList = {
            (uint8)EMarkerSecondary::C
          , (uint8)EMarkerSecondary::B
          , (uint8)EMarkerSecondary::A
          , (uint8)EMarkerSecondary::A | (uint8)EMarkerSecondary::B
           ,
        };

        uint32 Iterator = 0;
        while (Iterator++ < NUM_PLAYERS)
        {
            FPlayer Player;
            Player.Name            = Names[RandName(genName)];
            Player.PrimaryMarker   = static_cast<EMarkerPrimary>(RandPrimary(genPrimary));
            Player.SecondaryMarker = static_cast<EMarkerSecondary>(SecondaryList[RandSecondary(genSecondary)]);
            Players.push_back(Player);
        }
    }

    void SortPlayers()
    {
        struct MyPredicate
        {
            bool operator()(const FPlayer& a, const FPlayer& b) const
            {
                //Primary
                if (a.PrimaryMarker < b.PrimaryMarker)
                    return false;
                else if (a.PrimaryMarker > b.PrimaryMarker)
                    return true;

                uint8 a_SecondaryMarker = (uint8)a.SecondaryMarker;
                uint8 b_SecondaryMarker = (uint8)b.SecondaryMarker;
                //Secondary
                if (SecondaryMarkerOrderConfig[a_SecondaryMarker] > SecondaryMarkerOrderConfig[b_SecondaryMarker])
                    return true;
                if (SecondaryMarkerOrderConfig[a_SecondaryMarker] < SecondaryMarkerOrderConfig[b_SecondaryMarker])
                    return false;

                //Alphabetically
                if (a.Name < b.Name)
                    return true;
                return false;
            }
        };

        std::sort(Players.begin(), Players.end(), MyPredicate());
    }

    void PrintPlayers()
    {
        TArray<FString> PrimaryTexts   = { "a", "b" };
        TArray<FString> SecondaryTexts = { "A", "B", "C", "D" };

        for (const FPlayer& Player : Players)
        {
            const FString Primary   = PrimaryTexts[static_cast<uint8>(Player.PrimaryMarker)];
            const FString Secondary = SecondaryTexts[static_cast<uint8>(Player.SecondaryMarker)];
            const FString Name      = Player.Name;
            std::cout << "Primary: " << Primary << " | Secondary: " << Secondary << " | Name: " << Name << std::endl;
        }
    }
};
