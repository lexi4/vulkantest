﻿#pragma once
#include "Core/CoreMinimal.h"

class ISandbox
{
public:
    virtual ~ISandbox() = default;
    virtual void Execute() = 0;

public:

    template<class T>
    void SetSanboxTestName(T* InTestCase)
    {
        TypeName = typeName::type_name<decltype(InTestCase)>();
    }
    FTypeName TypeName;
};