﻿#include "DelegateTest.h"

#include "Button.h"
#include "ColorfulButton.h"

#include "Core/CoreMinimal.h"

void UDelegateTest::Execute()
{
    InitButton(ColorfulButton);
    InitButton(ShitButton);

    ColorfulButton->Writeshit();
    ShitButton->MakeShit();

    TArray<IButton*> Buttons {
        Cast<IButton>(ColorfulButton),
        Cast<IButton>(ShitButton),
    };

    for(IButton* Button : Buttons)
    {
        Button->Click();
        Button->Hide();
        Button->Show();
        Button->Kill();
    }
}

void UDelegateTest::OnClickHandler()
{
    std::cout << "void OnClickHandler()" << std::endl;
}

void UDelegateTest::OnCallHandler(float b, int c)
{
    std::cout << "void OnCallHandler : " << b << " , " << c << std::endl;
}

bool UDelegateTest::CheckAny(const EMyEnum& InEnum, const TArray<EMyEnum>& InValues)
{
    for(auto InValue : InValues)
    {
        if(InEnum == InValue)
        {
            return true;
        }
    }
    return false;
}

template <class T>
void UDelegateTest::InitButton(T*& ClassInstancePointer)
{
    InitObject(ClassInstancePointer);
    if(IButton* Button = Cast<IButton>(ClassInstancePointer))
    {
        Button->OnClick += DelegateLib::MakeDelegate(this, &UDelegateTest::OnClickHandler);
    }
}
