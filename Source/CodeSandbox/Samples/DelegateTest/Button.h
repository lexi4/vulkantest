﻿#pragma once

#include "Core/CoreMinimal.h"

class IButton
{
public:
    DelegateLib::MulticastDelegate0 OnClick;
    DelegateLib::MulticastDelegate2<float, int> OnCall;

public:
    virtual void Click() = 0;
    virtual void Show() = 0;
    virtual void Hide() = 0;
    virtual void Kill() = 0;
};
