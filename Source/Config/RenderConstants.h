﻿#pragma once

#include "Core/CoreMinimal.h"
#include "Render/Data/ERenderAPI.h"

namespace CONST_RENDER
{
   static const ERenderAPI RENDER_API = ERenderAPI::Vulkan;

   namespace WINDOW
   {
      static FVector2D SIZE = {800, 600};
      static FString NAME = "VulkanTest";
   }

   namespace VULKAN
   {
      static const bool ENABLED_VALIDATION_LAYERS = true;
   }
}
