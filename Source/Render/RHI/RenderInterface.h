﻿#pragma once

class UWindow;

class IRender
{
public:
    virtual ~IRender() = default;
    virtual void Init(UWindow* InWindow) = 0;
    virtual void RenderFrame() = 0;
};
