#pragma once
#include "VulkanRHI.h"

#include <GLFW/glfw3.h>

#include "Config/RenderConstants.h"
#include "Render/Window/Window.h"

PFN_vkCreateDebugUtilsMessengerEXT  pfnVkCreateDebugUtilsMessengerEXT;
PFN_vkDestroyDebugUtilsMessengerEXT pfnVkDestroyDebugUtilsMessengerEXT;

VKAPI_ATTR VkResult VKAPI_CALL vkCreateDebugUtilsMessengerEXT( VkInstance                                 instance,
                                                               const VkDebugUtilsMessengerCreateInfoEXT * pCreateInfo,
                                                               const VkAllocationCallbacks *              pAllocator,
                                                               VkDebugUtilsMessengerEXT *                 pMessenger )
{
    LOG();
    return pfnVkCreateDebugUtilsMessengerEXT( instance, pCreateInfo, pAllocator, pMessenger );
}

VKAPI_ATTR void VKAPI_CALL vkDestroyDebugUtilsMessengerEXT( VkInstance                    instance,
                                                            VkDebugUtilsMessengerEXT      messenger,
                                                            VkAllocationCallbacks const * pAllocator )
{
    LOG();
    return pfnVkDestroyDebugUtilsMessengerEXT( instance, messenger, pAllocator );
}

VKAPI_ATTR VkBool32 VKAPI_CALL debugMessageFunc( VkDebugUtilsMessageSeverityFlagBitsEXT       messageSeverity,
                                                 VkDebugUtilsMessageTypeFlagsEXT              messageTypes,
                                                 VkDebugUtilsMessengerCallbackDataEXT const * pCallbackData,
                                                 void * /*pUserData*/ )
{
  FString message;

  message += vk::to_string( static_cast<vk::DebugUtilsMessageSeverityFlagBitsEXT>( messageSeverity ) ) + ": " +
             vk::to_string( static_cast<vk::DebugUtilsMessageTypeFlagsEXT>( messageTypes ) ) + ":\n";
  message += FString( "\t" ) + "messageIDName   = <" + pCallbackData->pMessageIdName + ">\n";
  message += FString( "\t" ) + "messageIdNumber = " + std::to_string( pCallbackData->messageIdNumber ) + "\n";
  message += FString( "\t" ) + "message         = <" + pCallbackData->pMessage + ">\n";
  if ( 0 < pCallbackData->queueLabelCount )
  {
    message += FString( "\t" ) + "Queue Labels:\n";
    for ( uint8_t i = 0; i < pCallbackData->queueLabelCount; i++ )
    {
      message += FString( "\t\t" ) + "labelName = <" + pCallbackData->pQueueLabels[i].pLabelName + ">\n";
    }
  }
  if ( 0 < pCallbackData->cmdBufLabelCount )
  {
    message += FString( "\t" ) + "CommandBuffer Labels:\n";
    for ( uint8_t i = 0; i < pCallbackData->cmdBufLabelCount; i++ )
    {
      message += FString( "\t\t" ) + "labelName = <" + pCallbackData->pCmdBufLabels[i].pLabelName + ">\n";
    }
  }
  if ( 0 < pCallbackData->objectCount )
  {
    for ( uint8_t i = 0; i < pCallbackData->objectCount; i++ )
    {
      message += FString( "\t" ) + "Object " + std::to_string( i ) + "\n";
      message += FString( "\t\t" ) + "objectType   = " +
                 vk::to_string( static_cast<vk::ObjectType>( pCallbackData->pObjects[i].objectType ) ) + "\n";
      message +=
        FString( "\t\t" ) + "objectHandle = " + std::to_string( pCallbackData->pObjects[i].objectHandle ) + "\n";
      if ( pCallbackData->pObjects[i].pObjectName )
      {
        message += FString( "\t\t" ) + "objectName   = <" + pCallbackData->pObjects[i].pObjectName + ">\n";
      }
    }
  }
  LOG(message);
  return false;
}

namespace AppInfo
{
    static FString AppName = CONST_RENDER::WINDOW::NAME;
    static uint32 AppVersion = VK_MAKE_VERSION(0, 0, 1);
    static FString EngineName = "Pursuit";
    static uint32 EngineVersion = VK_MAKE_VERSION(0, 0, 1);
    static uint32 VulkanVersion = VK_API_VERSION_1_2;
}

static TArray<FString> VulkanExtensions;
static TArray<FString> VulkanLayers;

vk::Instance UVulkanRHI::InitVulkanInstance(const TArray<FString>& Extensions,
    const TArray<FString>& ValidationLayers)
{
    LOG();
    vk::ApplicationInfo ApplicationInfo
    (
        AppInfo::AppName.c_str(),
        AppInfo::AppVersion,
        AppInfo::EngineName.c_str(),
        AppInfo::EngineVersion,
        AppInfo::VulkanVersion
    );

    TArray<char const*> RequestedExtensions;
    for (auto const& ExtensionName : Extensions)
    {
        RequestedExtensions.push_back(ExtensionName.data());
    }

    vk::InstanceCreateInfo InstanceCreateInfo
    (
        vk::InstanceCreateFlags(),
        &ApplicationInfo,
        {},
        RequestedExtensions
    );


    return createInstance(InstanceCreateInfo);
}

void UVulkanRHI::InitValidationLayers()
{
    if(!CONST_RENDER::VULKAN::ENABLED_VALIDATION_LAYERS)
    {
        return;
    }
    LOG();

    pfnVkCreateDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>( VulkanInstance.getProcAddr( "vkCreateDebugUtilsMessengerEXT" ) );
    if ( !pfnVkCreateDebugUtilsMessengerEXT )
    {
        std::cerr << "GetInstanceProcAddr: Unable to find pfnVkCreateDebugUtilsMessengerEXT function." << std::endl;
        exit( 1 );
    }

    pfnVkDestroyDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(
      VulkanInstance.getProcAddr( "vkDestroyDebugUtilsMessengerEXT" ) );
    if ( !pfnVkDestroyDebugUtilsMessengerEXT )
    {
        std::cerr << "GetInstanceProcAddr: Unable to find pfnVkDestroyDebugUtilsMessengerEXT function." << std::endl;
        exit( 1 );
    }

    vk::DebugUtilsMessageSeverityFlagsEXT severityFlags( vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
                                                         vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo |
                                                         vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
                                                         vk::DebugUtilsMessageSeverityFlagBitsEXT::eError );
    vk::DebugUtilsMessageTypeFlagsEXT messageTypeFlags( vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
                                                        vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance |
                                                        vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation );

    vk::DebugUtilsMessengerCreateInfoEXT DebugUtilsMessengerCreateInfo = { {}, severityFlags, messageTypeFlags, &debugMessageFunc };
    vk::UniqueDebugUtilsMessengerEXT debugUtilsMessenger = VulkanInstance.createDebugUtilsMessengerEXTUnique(DebugUtilsMessengerCreateInfo);
}

void UVulkanRHI::EnumerateDevices()
{
    LOG();
    VulkanPhysicalDevices = VulkanInstance.enumeratePhysicalDevices();
    std::cout << "== Devices BEGIN ==" << std::endl;
    uint32 count = 0;
    for (auto Device : VulkanPhysicalDevices)
    {
        std::cout << "- GPU "<< count <<" BEGIN -" << std::endl;
        vk::PhysicalDeviceProperties props;
        props = Device.getProperties();
        std::cout
        << "  " << props.deviceName << " | Type: " << to_string(props.deviceType) << std::endl
        << "  Driver: " << VK_VERSION_MAJOR(props.driverVersion) << "." <<VK_VERSION_MINOR(props.driverVersion) << std::endl
        << "  Vulkan API: " << VK_VERSION_MAJOR(props.apiVersion) << "." <<VK_VERSION_MINOR(props.apiVersion) << "." << VK_VERSION_PATCH(props.apiVersion) << std::endl
        << "    - Allocation count: " << props.limits.maxMemoryAllocationCount << std::endl
        << "    - Compute shared size: " << props.limits.maxComputeSharedMemorySize << std::endl
        << "    - FrameBuffer: " << props.limits.maxFramebufferWidth << "x" << props.limits.maxFramebufferHeight << " | Layers: " << props.limits.maxFramebufferLayers << std::endl
        << "- GPU "<< count++ <<" END -" << std::endl;
    }
    std::cout << "== Devices END ==" << std::endl;
}

UVulkanRHI::~UVulkanRHI()
{
    LOG();
}

void UVulkanRHI::Init(UWindow* InWindow)
{
    LOG();
    uint32 ExtensionCount = 0;
    TArray<FString>* GlfwExtensions = CharsToStrings(glfwGetRequiredInstanceExtensions(&ExtensionCount));
    VulkanExtensions.insert(VulkanExtensions.end(), GlfwExtensions->begin(), GlfwExtensions->end());
    delete GlfwExtensions;

    if(CONST_RENDER::VULKAN::ENABLED_VALIDATION_LAYERS)
    {
        VulkanExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        VulkanLayers.push_back("VK_LAYER_KHRONOS_validation");
    }

    VulkanInstance = InitVulkanInstance(VulkanExtensions, VulkanLayers);
    InitValidationLayers();
    EnumerateDevices();

    auto Surface_CType = static_cast<VkSurfaceKHR>(VulkanSurface);
    glfwCreateWindowSurface(static_cast<VkInstance>(VulkanInstance), InWindow->GlfwWindow, nullptr, &Surface_CType);
}

void UVulkanRHI::RenderFrame()
{
    LOG();
    return;
}
