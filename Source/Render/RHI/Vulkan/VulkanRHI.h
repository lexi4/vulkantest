#pragma once

#include <Core/CoreMinimal.h>

#define VULKAN_HPP_NO_NODISCARD_WARNINGS
#include <vulkan/vulkan.hpp>

#include "Render/RHI/RenderInterface.h"

class UVulkanRHI : public IRender
{
public:
    void Init(UWindow* InWindow) override;
    void RenderFrame() override;
    ~UVulkanRHI() override;

private:
    vk::Instance InitVulkanInstance(const TArray<FString>& Extensions, const TArray<FString>& ValidationLayers);
    void InitValidationLayers();
    void EnumerateDevices();

private:
    vk::SurfaceKHR VulkanSurface = nullptr;
    vk::Instance VulkanInstance = nullptr;
    TArray<vk::PhysicalDevice> VulkanPhysicalDevices = TArray<vk::PhysicalDevice>();
};

