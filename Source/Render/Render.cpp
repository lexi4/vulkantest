﻿#include "Render.h"

#include "Config/RenderConstants.h"
#include "RHI/Vulkan/VulkanRHI.h"
#include "Data/ERenderAPI.h"
#include "Window/Window.h"
#include "Render/RHI/RenderInterface.h"

URender::URender()
{
    LOG()
    Window = new UWindow();

    switch (CONST_RENDER::RENDER_API)
    {
        case ERenderAPI::Vulkan:
            {
                UVulkanRHI* Vulkan = new UVulkanRHI();
                RenderHardwareInterface = StaticCast<IRender>(Vulkan);
            }
        break;
        default:
            std::cerr << "[URender::URender] Invalid RENDER_API value!" << std::endl;
    }
    RenderHardwareInterface->Init(Window);
}

void URender::RenderFrame() const
{
    LOG()
    RenderHardwareInterface->RenderFrame();
}

URender::~URender()
{
    LOG()
    delete RenderHardwareInterface;
    delete Window;
}
