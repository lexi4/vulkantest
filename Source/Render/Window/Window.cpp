﻿#include "Window.h"

#include <GLFW/glfw3.h>

#include "Config/RenderConstants.h"

UWindow::UWindow()
{
    LOG();
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    const FVector2D& WindowSize = CONST_RENDER::WINDOW::SIZE;
    const FString& WindowName = CONST_RENDER::WINDOW::NAME;
    GlfwWindow = glfwCreateWindow((int)WindowSize.X, (int)WindowSize.Y, WindowName.c_str(), nullptr, nullptr);
}

UWindow::~UWindow()
{
    LOG();
    glfwDestroyWindow(GlfwWindow);
    glfwTerminate();
    GlfwWindow = nullptr;
}
