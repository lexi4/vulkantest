﻿#pragma once
#include "Core/CoreMinimal.h"

struct GLFWwindow;

class UWindow
{
public:
    UWindow();
    ~UWindow();

public:
    GLFWwindow* GlfwWindow = nullptr;
};
